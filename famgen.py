#Generates fantasy names; some have two lists, like the dwarves, as they have unique-sounding last names.  Others, like the elf lists, just pull from the same list and mash them together.  Feel free to replace or update the lists I have; I pulled them from allllll over the internet, but there's likely to be some stupid ones.
import random
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-r", "--race", help="character race; options are halfling, dwarf, human, elf, helf(half-elf), orc, horc(half-orc), dragonborn, and lizardfolk")
arg = parser.parse_args()
name = arg.race
#this function reads the files and pulls out a last name and a random amount of first names for relatives.
def nameGen():
#	print ("we still have", nameFile)
	givenname = (random.choice(open(gnameFile).read().split()))
	surname = (random.choice(open(snameFile).read().split()))
	print (givenname + " " + surname)
	from random import randint
	offsp = randint(0,4)
	for i in range(offsp):
		relgiv = (random.choice(open(gnameFile).read().split()))
		print(relgiv + " " + surname)
#tried a switch statement, but apparently python was created by people that don't care about the amount of typing they do
if(name == "halfling"):
    gnameFile = "./namelists/halflist.txt"
    snameFile = "./namelists/halfLlist.txt"
elif(name == "dwarf"):
    gnameFile = "./namelists/dwarflist.txt"
    snameFile = "./namelists/dwarfLlist.txt"
elif(name == ("elf")):
    gnameFile = "./namelists/elflist.txt"
    snameFile = "./namelists/elflist.txt"
elif(name == "helf"):
    gnameFile = "./namelists/helflist.txt"
    snameFile = "./namelists/helfLlist.txt"
elif(name == "human"):
    gnameFile = "./namelists/humlist.txt"
    snameFile = "./namelists/humLlist.txt"
elif(name == "orc"):
    gnameFile = "./namelists/orclist.txt"
    snameFile = "./namelists/orcLlist.txt"
elif(name == "horc"):
    gnameFile = "./namelists/horclist.txt"
    snameFile = "./namelists/horcLlist.txt"
elif(name == "dragonborn"):
    gnameFile = "./namelists/dbornlist.txt"
    snameFile = "./namelists/dbornLlist.txt"
elif(name == "lizardfolk"):
    gnameFile = "./namelists/lizlist.txt"
    snameFile = "./namelists/lizlist.txt"
else:
    print('Sorry, you have to provide a valid race.  For help, type "python famgen.py -h"')

#print ("nameFile is", nameFile)

nameGen()

